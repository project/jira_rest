<?php

namespace Drupal\jira_rest;

use Drupal\Core\Config\ConfigValueException;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\key\KeyRepositoryInterface;
use JiraRestApi\Configuration\ArrayConfiguration;
use JiraRestApi\Issue\IssueService;
use JiraRestApi\Project\ProjectService;
use JiraRestApi\JiraException;
use JiraRestApi\User\UserService;

/**
 * Class JiraRestWrapperService.
 *
 * @package Drupal\jira_rest
 */
class JiraRestWrapperService {
  use StringTranslationTrait;

  /**
   * The JIRA Endpoint Config Object.
   *
   * @var \Drupal\jira_rest\JiraEndpointRepositoryInterface
   */
  protected $endpointRepository;

  /**
   * The key repository.
   *
   * @var \Drupal\key\KeyRepositoryInterface
   */
  protected $keyRepository;

  /**
   * @var \Drupal\Core\Logger\LoggerChannelInterface
   */
  protected $loggerRestJira;

  /**
   * JiraRestWrapper constructor.
   *
   * @param \Drupal\jira_rest\JiraEndpointRepositoryInterface $endpoint_repository
   *   JIRA Endpoint Repository service.
   * @param \Drupal\key\KeyRepositoryInterface $key_repository
   *   Key Repository service.
   * @param \Drupal\Core\Logger\LoggerChannelFactoryInterface $logger_factory
   *   Logger Factory service.
   *
   * @throws \Exception
   */
  public function __construct(JiraEndpointRepositoryInterface $endpoint_repository, KeyRepositoryInterface $key_repository, LoggerChannelFactoryInterface $logger_factory, $endpoint_id = NULL) {
    $this->endpointRepository = $endpoint_repository;
    $this->keyRepository = $key_repository;
    $this->loggerRestJira = $logger_factory->get('jira_rest');
  }

  /**
   * @param \Drupal\jira_rest\JiraEndpointInterface $jira_endpoint
   *
   * @return \JiraRestApi\Configuration\ArrayConfiguration
   *   An array of JIRA configuration settings.
   *
   * @throws \Exception
   */
  protected function getArrayConfiguration(JiraEndpointInterface $jira_endpoint) {
    $credentials = $jira_endpoint->getCredentials();
    $credential_provider = $jira_endpoint->getCredentialProvider();
    $credential = $credentials[$credential_provider];

    $username = $this->getUsernameByCredentialType($credential_provider, $credential);
    if(!$username) {
      throw new ConfigValueException($this->t('The username value cannot be empty on @label.', [
        '@label' => $jira_endpoint->label(),
      ]));
    }

    $password = $this->getPasswordByCredentialType($credential_provider, $credential);
    if(!$password) {
      throw new ConfigValueException($this->t('The password value cannot be empty on @label.', [
        '@label' => $jira_endpoint->label(),
      ]));
    }

    return new ArrayConfiguration([
      'jiraHost' => $jira_endpoint->getInstanceUrl(),
      'jiraUser' => $username,
      'jiraPassword' => $password,
      'useV3RestApi' => ($jira_endpoint->getApiVersion() == '3'),
    ]);
  }

  /**
   * @param string $credential_provider
   * @param array $credential
   *
   * @return string
   */
  public function getUsernameByCredentialType(string $credential_provider, array $credential): string {
    $username = '';

    switch ($credential_provider) {
      case "jira_rest":
      case "key_password_only":
        $username = $credential['username'];
        break;
      case "key":
        /** @var \Drupal\key\KeyInterface $key */
        if ($key_username = $this->keyRepository->getKey($credential['username'])) {
          if ($key_value = $key_username->getKeyValue()) {
            $username = $key_value;
          }
        }
        break;
      case "multikey":
        /** @var \Drupal\key\KeyInterface $multikey */
        if ($multikey = $this->keyRepository->getKey($credential['user_password'])) {
          $key_values = $multikey->getKeyValues();
          $username = $key_values['username'] ?? '';
        }
        break;
      default:
        $username = '';
    }

    return $username;
  }

  /**
   * @param string $credential_provider
   * @param array $credential
   *
   * @return string
   */
  public function getPasswordByCredentialType(string $credential_provider, array $credential): string {
    $password = '';
    switch ($credential_provider) {
      case "jira_rest":
        $password = $credential['password'];
        break;
      case "key":
      case "key_password_only":
        /** @var \Drupal\key\KeyInterface $key */
        if ($key_password = $this->keyRepository->getKey($credential['password'])) {
          if ($key_value = $key_password->getKeyValue()) {
            $password = $key_value;
          }
        }
        break;
      case "multikey":
        /** @var \Drupal\key\KeyInterface $multikey */
        if ($multikey = $this->keyRepository->getKey($credential['user_password'])) {
          $key_values = $multikey->getKeyValues();
          $password = $key_values['password'] ?? '';
        }
        break;
      default:
        $password = '';
    }

    return $password;
  }

  /**
   * Get the Issue service api.
   *
   * @return \JiraRestApi\Issue\IssueService
   *   Issue Service API.
   * @throws \JiraRestApi\JiraException
   * @throws \JsonMapper_Exception
   * @throws \Exception
   */
  public function getIssueService($endpoint_id = NULL) {
    // Attempt to get a specific endpoint
    if (!empty($endpoint_id) ) {
      $endpoint = $this->endpointRepository->getEndpoint($endpoint_id);
    }
    if (!isset($endpoint)) {
        $endpoint = $this->endpointRepository->getDefaultEndpoint();
    }

    if (empty($endpoint)) {
      throw new JiraException($this->t('No JIRA Endpoints could be found.'));
    }

    // Initialize the JIRA Issue Service
    try {
      $issueService = new IssueService($this->getArrayConfiguration($endpoint));
    } catch (JiraException $e) {
      $this->loggerRestJira->error($e->getMessage());
    }
    return $issueService;
  }

  /**
   * Get the Project service api.
   *
   * @return \JiraRestApi\Project\ProjectService
   *   Project Service API.
   * @throws \JiraRestApi\JiraException
   * @throws \JsonMapper_Exception
   * @throws \Exception
   */
  public function getProjectService($endpoint_id = NULL) {
    // Attempt to get a specific endpoint
    if (!empty($endpoint_id) ) {
      $endpoint = $this->endpointRepository->getEndpoint($endpoint_id);
    }
    if (!isset($endpoint)) {
        $endpoint = $this->endpointRepository->getDefaultEndpoint();
    }

    if (empty($endpoint)) {
      throw new JiraException($this->t('No JIRA Endpoints could be found.'));
    }

    // Initialize the JIRA Project Service
    try {
      $projectService = new ProjectService($this->getArrayConfiguration($endpoint));
    } catch (JiraException $e) {
      $this->loggerRestJira->error($e->getMessage());
    }
    return $projectService;
  }

  /**
   * Get the User service api.
   *
   * @return \JiraRestApi\User\UserService
   *   User Service API.
   * @throws \JiraRestApi\JiraException
   * @throws \JsonMapper_Exception
   * @throws \Exception
   */
  public function getUserService($endpoint_id = NULL) {
    // Attempt to get a specific endpoint
    if (!empty($endpoint_id) ) {
      $endpoint = $this->endpointRepository->getEndpoint($endpoint_id);
    }
    if (!isset($endpoint)) {
      $endpoint = $this->endpointRepository->getDefaultEndpoint();
    }

    if (empty($endpoint)) {
      throw new JiraException($this->t('No JIRA Endpoints could be found.'));
    }

    // Initialize the JIRA User Service
    try {
      $userService = new UserService($this->getArrayConfiguration($endpoint));
    } catch (JiraException $e) {
      $this->loggerRestJira->error($e->getMessage());
    }
    return $userService;
  }
}
