<?php

namespace Drupal\jira_rest;

use Drupal\Core\Config\Entity\ConfigEntityInterface;

/**
 * Provides an interface defining a JIRA Endpoint config entity.
 */
interface JiraEndpointInterface extends ConfigEntityInterface {

  /**
   * Returns the credential provider type.
   *
   * @return string
   *   The credential provider type.
   */
  public function getCredentialProvider();

  /**
   * Sets the credentials.
   *
   * @param array $credentials
   *   The credentials.
   *
   * @return $this
   */
  public function setCredentials(array $credentials);

  /**
   * Returns the credentials.
   *
   * @return array
   *   The credentials.
   */
  public function getCredentials();

  /**
   * Returns the JIRA endpoint URL.
   *
   * @return string
   *   The JIRA endpoint URL.
   */
  public function getInstanceUrl();

  /**
   * Returns the issue close transition ID.
   *
   * @return integer
   *   The transition ID to close an issue.
   */
  public function getCloseTransitionId();

  /**
   * Returns the issue resolution transition ID.
   *
   * @return integer
   *   The transition ID to resolve an issue.
   */
  public function getResolveTransitionId();

  /**
   * Returns the API version of the JIRA endpoint.
   *
   * @return string
   *   The API version of the JIRA endpoint.
   */
  public function getApiVersion();

}
