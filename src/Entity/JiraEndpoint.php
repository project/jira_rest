<?php

namespace Drupal\jira_rest\Entity;

use Drupal\Core\Config\Entity\ConfigEntityBase;
use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Entity\EntityWithPluginCollectionInterface;
use Drupal\jira_rest\JiraEndpointInterface;
use Drupal\key\Exception\KeyValueNotSetException;
use Drupal\key\KeyInterface;
use Drupal\key\Plugin\KeyPluginCollection;
use Drupal\key\Plugin\KeyProviderSettableValueInterface;

/**
 * Defines the Key entity.
 *
 * @ConfigEntityType(
 *   id = "jira_endpoint",
 *   label = @Translation("Jira Endpoint"),
 *   module = "jira_rest",
 *   handlers = {
 *     "list_builder" = "Drupal\jira_rest\Controller\JiraEndpointListBuilder",
 *     "form" = {
 *       "add" = "Drupal\jira_rest\Form\JiraEndpointAddForm",
 *       "edit" = "Drupal\jira_rest\Form\JiraEndpointEditForm",
 *       "delete" = "Drupal\jira_rest\Form\JiraEndpointDeleteForm"
 *     },
 *   },
 *   config_prefix = "jira_endpoint",
 *   admin_permission = "administer jira_rest",
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "label"
 *   },
 *   links = {
 *     "add-form" = "/admin/config/services/jira_rest/add",
 *     "edit-form" = "/admin/config/services/jira_rest/manage/{jira_endpoint}",
 *     "delete-form" = "/admin/config/services/jira_rest/manage/{jira_endpoint}/delete",
 *     "collection" = "/admin/config/services/jira_rest"
 *   },
 *   config_export = {
 *     "id",
 *     "label",
 *     "description",
 *     "instanceurl",
 *     "credential_provider",
 *     "credentials",
 *     "api_version",
 *     "close_issue_transititon_id",
 *     "resolve_issue_transition_id",
 *   }
 * )
 */
class JiraEndpoint extends ConfigEntityBase implements JiraEndpointInterface {

  /**
   * The JIRA endpoint ID.
   *
   * @var string
   */
  protected $id;

  /**
   * The JIRA endpoint label.
   *
   * @var string
   */
  protected $label;

  /**
   * The credential provider type.
   *
   * @var string
   */
  protected $credential_provider;

  /**
   * @var array
   */
  protected $credentials;

  /**
   * The JIRA endpoint URL.
   *
   * @var string
   */
   protected $instanceurl;

   /**
    * The close issue transition ID.
    *
    * @var integer
    */
   protected $close_issue_transition_id = 2;

   /**
    * The resolve issue transition ID.
    *
    * @var integer
    */
   protected $resolve_issue_transition_id = 5;

   /**
    * The API version to use when connecting to the JIRA endpoint.
    *
    * @var string
    */
   protected $api_version;

  /**
   * {@inheridoc}
   */
  public function getCredentialProvider() {
    return $this->credential_provider;
  }

  /**
   * {@inheridoc}
   */
  public function setCredentials(array $credentials) {
    $this->credentials = $credentials;
  }

  /**
   * {@inheridoc}
   */
  public function getCredentials() {
    return $this->credentials;
  }

  /**
   * {@inheridoc}
   */
  public function getInstanceUrl() {
    return $this->instanceurl;
  }

  /**
   * {@inheridoc}
   */
  public function getCloseTransitionId() {
    return $this->close_issue_transition_id;
  }

  /**
   * {@inheridoc}
   */
  public function getResolveTransitionId() {
    return $this->resolve_issue_transition_id;
  }

  /**
   * {@inheridoc}
   */
  public function getApiVersion() {
    return $this->api_version ?: '2';
  }

}
