<?php

namespace Drupal\jira_rest\Form;

use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Ajax\HtmlCommand;
use Drupal\Core\Entity\EntityForm;
use Drupal\Core\Config\Entity\ConfigEntityStorageInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\jira_rest\JiraRestWrapperService;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\ClientException;
use Drupal\Component\Serialization\Json;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Base form for Jira Endpoint add and edit forms.
 */
abstract class JiraEndpointFormBase extends EntityForm {

  /**
   * The key storage.
   *
   * @var \Drupal\Core\Entity\EntityStorageInterface
   */
  protected $storage;

  /**
   * The HTTP client.
   *
   * @var \GuzzleHttp\Client
   */
  protected $httpClient;

  /**
   * @var \Drupal\jira_rest\JiraRestWrapperService
   */
  protected $jiraRestWrapper;

  /**
   * Constructs a new jira endpoint form base.
   *
   * @param \Drupal\Core\Config\Entity\ConfigEntityStorageInterface $storage
   *   The Jira Endpoint storage.
   * @param \GuzzleHttp\Client $http_client
   *   The Guzzle HTTP client for REST Requests.
   * @param \Drupal\jira_rest\JiraRestWrapperService
   *   The Jira rest wrapper service.
   */
  public function __construct(ConfigEntityStorageInterface $storage, Client $http_client, JiraRestWrapperService $jira_rest_wrapper) {
    $this->storage = $storage;
    $this->httpClient = $http_client;
    $this->jiraRestWrapper = $jira_rest_wrapper;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('entity_type.manager')->getStorage('jira_endpoint'),
      $container->get('http_client'),
      $container->get('jira_rest_wrapper_service')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function form(array $form, FormStateInterface $form_state) {
    /* @var $jira_endpoint \Drupal\jira_rest\Entity\JiraEndpoint */
    $jira_endpoint = $this->entity;

    $form['label'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Instance Label'),
      '#maxlength' => 255,
      '#default_value' => $jira_endpoint->label(),
      '#required' => TRUE,
    ];
    $form['id'] = [
      '#type' => 'machine_name',
      '#default_value' => $jira_endpoint->id(),
      '#machine_name' => [
        'exists' => [$this->storage, 'load'],
      ],
      '#disabled' => !$jira_endpoint->isNew(),
    ];

    $credential_provider_value = $jira_endpoint->getCredentialProvider() ?? 'jira_rest';

    $form['credential_provider'] = [
      '#type' => 'select',
      '#title' => $this->t('Credential provider'),
      '#options' => [
        'jira_rest' => 'Jira rest',
      ],
      '#default_value' => $credential_provider_value,
      '#description' => $this->t('For security reasons it is recommended to use the key module.'),
      '#ajax' => [
        'callback' => [$this, 'ajaxCallback'],
        'wrapper' => 'credentials',
        'method' => 'replace',
        'effect' => 'fade',
      ],
    ];

    $credential_provider = $form_state->getValue('credential_provider', $credential_provider_value);

    if (\Drupal::moduleHandler()->moduleExists('key')) {
      $form['credential_provider']['#options']['key'] = 'Key Module';
      $form['credential_provider']['#options']['key_password_only'] = 'Key Module (password only)';
      /** @var \Drupal\key\Plugin\KeyPluginManager $key_type */
      $key_type = \Drupal::service('plugin.manager.key.key_type');
      if ($key_type->hasDefinition('user_password')) {
        $form['credential_provider']['#options']['multikey'] = 'Key Module (user/password)';
      }
    }

    $form['credentials'] = [
      '#type' => 'container',
      '#id' => 'credentials',
      '#tree' => TRUE,
    ];

    $credentials = $jira_endpoint->getCredentials();

    if ($credential_provider === 'jira_rest') {
      $form['credentials'][$credential_provider]['username'] = [
        '#type' => 'textfield',
        '#title' => $this->t('Username'),
        '#description' => $this->t('A username required by the Jira rest'),
        '#default_value' => $credentials[$credential_provider]['username'] ?? NULL,
        '#attributes' => [
          'autocomplete' => 'off',
        ],
      ];

      $current_password = $credentials[$credential_provider]['password'] ?? NULL;
      $form['credentials'][$credential_provider]['password'] = [
        '#type' => 'password',
        '#title' => $this->t('Password'),
        '#description' => $this->t('A password required by the Jira rest.'),
        '#default_value' => $current_password,
        '#attributes' => [
          'autocomplete' => 'off',
        ],
      ];

      if (!empty($current_password)) {
        $form_state->set('current_password', $current_password);
        $form['credentials'][$credential_provider]['password']['#description'] = $this->t('A password required by the Jira host. <em>The currently set password is hidden for security reasons</em>.');
      }

    }
    elseif ($credential_provider === 'key') {
      $form['credentials'][$credential_provider]['username'] = [
        '#type' => 'key_select',
        '#title' => $this->t('Username'),
        '#description' => $this->t('A username required by the Jira host.'),
        '#default_value' => $jira_endpoint->get('credentials.key.username'),
        '#empty_option' => $this->t('- Please select -'),
        '#key_filters' => ['type' => 'authentication'],
        '#required' => TRUE,
      ];
      $form['credentials'][$credential_provider]['password'] = [
        '#type' => 'key_select',
        '#title' => $this->t('Password'),
        '#description' => $this->t('A password required by the Jira host.'),
        '#default_value' => $credentials[$credential_provider]['password'] ?? NULL,
        '#empty_option' => $this->t('- Please select -'),
        '#key_filters' => ['type' => 'authentication'],
        '#required' => TRUE,
      ];
    }
    elseif ($credential_provider === 'key_password_only') {
      $form['credentials'][$credential_provider]['username'] = [
        '#type' => 'textfield',
        '#title' => $this->t('Username'),
        '#description' => $this->t('A username required by the Jira rest'),
        '#default_value' => $credentials[$credential_provider]['username'] ?? NULL,
        '#attributes' => [
          'autocomplete' => 'off',
        ],
        '#required' => TRUE,
      ];
      $form['credentials'][$credential_provider]['password'] = [
        '#type' => 'key_select',
        '#title' => $this->t('Password'),
        '#description' => $this->t('A password required by the Jira host.'),
        '#default_value' => $credentials[$credential_provider]['password'] ?? NULL,
        '#empty_option' => $this->t('- Please select -'),
        '#key_filters' => ['type' => 'authentication'],
        '#required' => TRUE,
      ];
    }
    elseif ($credential_provider === 'multikey') {
      $form['credentials'][$credential_provider]['user_password'] = [
        '#type' => 'key_select',
        '#title' => $this->t('User/password'),
        '#description' => $this->t('A username + password required by the Jira host.'),
        '#default_value' => $credentials[$credential_provider]['user_password'] ?? NULL,
        '#empty_option' => $this->t('- Please select -'),
        '#key_filters' => ['type' => 'user_password'],
        '#required' => TRUE,
      ];
    }

    $form['instanceurl'] = [
      '#type' => 'textfield',
      '#title' => $this->t('URL of the JIRA instance'),
      '#default_value' => $jira_endpoint->getInstanceUrl(),
      '#description' => $this->t("Enter the URL of your JIRA instance (e.g. https://yourjira.com:8443)"),
      '#required' => TRUE,
    ];

    $form['api_version'] = [
      '#type' => 'select',
      '#title' => $this->t('JIRA REST API version'),
      '#description' => $this->t('API v3 is required to connect to JIRA Cloud instances, but please note, that not all V3 API functions are supported.'),
      '#options' => [
        '2' => $this->t('API v@version', ['@version' => '2']),
        '3' => $this->t('API v@version', ['@version' => '3']),
      ],
      '#default_value' => $jira_endpoint->getApiVersion(),
      '#required' => TRUE,
    ];

    $form['close_issue_transition_id'] = [
      '#type' => 'number',
      '#title' => $this->t('the default transition ID to close an issue'),
      '#default_value' => $jira_endpoint->getCloseTransitionId(),
      '#size' => 4,
      '#description' => $this->t("Enter the default transition ID to close an issue with jira_rest_closeissuefixed()"),
      '#required' => TRUE,
      '#min' => 0,
      '#step' => 1,
    ];

    $form['resolve_issue_transition_id'] = [
      '#type' => 'number',
      '#title' => $this->t('default transition ID to resolve an issue'),
      '#default_value' => $jira_endpoint->getResolveTransitionId(),
      '#size' => 4,
      '#description' => $this->t("Enter the default transition ID to resolve an issue with jira_rest_resolveissuefixed()"),
      '#required' => TRUE,
      '#min' => 0,
      '#step' => 1,
    ];

    return parent::form($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    if ($form_state->getValue('credential_provider') == 'jira_rest') {
      if (empty($form_state->getValue(['credentials', 'jira_rest', 'password']))) {
        $password = $form_state->get('current_password');
        $form_state->setValue(['credentials', 'jira_rest', 'password'], $password);
      }
    }

    $jira_url = $form_state->getValue('instanceurl');
    if ((strpos(strrev($jira_url), strrev('/')) === 0)) {
      $form_state->setErrorByName('instanceurl', $this->t('URL must not end with "/"'));
    }

    parent::validateForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form =  parent::buildForm($form, $form_state);

    $form['jira_validation'] = [
      '#type' => 'markup',
      '#markup' => '<div id="jira-rest-validation"></div>'
    ];

    $form['actions']['validate'] = [
      '#type' => 'button',
      '#value' => $this->t('Validate'),
      '#attributes' => [
        'aria-label' => $this->t('Validate'),
        'event' => 'click',
      ],
      '#ajax' => [
        'callback' => [$this, 'ajaxJiraRestValidation'],
      ],
    ];

    return $form;
  }

  /**
   * @param array $form
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *
   * @return \Drupal\Core\Ajax\AjaxResponse
   */
  public function ajaxJiraRestValidation(array &$form, FormStateInterface $form_state) {
    if (!$form_state->hasAnyErrors()) {
      $form_values = $form_state->getValues();

      $endpoint_url = $form_values['instanceurl'] . "/rest/api/{$form_values['api_version']}/myself";

      $options = [
        'auth' => [
          $this->jiraRestWrapper->getUsernameByCredentialType(
            $form_values['credential_provider'],
            $form_values['credentials'][$form_values['credential_provider']]
          ),
          $this->jiraRestWrapper->getPasswordByCredentialType(
            $form_values['credential_provider'],
            $form_values['credentials'][$form_values['credential_provider']]
          )
        ],
        'headers'  => [
          'content-type' => 'application/json', 'Accept' => 'application/json'
        ],
        'connect_timeout' => 5,
      ];

      try {
        $client_response = $this->httpClient->get($endpoint_url, $options);
        $data = Json::decode($client_response->getBody()->getContents());
        $this->messenger()->addMessage($this->t(
          "Successful connected with <strong>@display_name</strong> with email address <strong>@email_address</strong>.",
          [
            '@display_name' => $data['displayName'] ?? '',
            '@email_address' => $data['emailAddress'] ?? '',
          ]
        ), 'status');


      } catch (ClientException $e) {
        $this->messenger()->addError($e->getMessage());
      }
    }

    $message = [
      '#theme' => 'status_messages',
      '#message_list' => $this->messenger()->all(),
    ];

    $response = new AjaxResponse();
    $response->addCommand(new HtmlCommand('#jira-rest-validation', ($message)));

    $this->messenger()->deleteAll();

    return $response;
  }

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state) {
    // Allow exceptions to percolate, per EntityFormInterface.
    $status = parent::save($form, $form_state);

    $t_args = ['%name' => $this->entity->label()];
    if ($status == SAVED_UPDATED) {
      $this->messenger()->addStatus($this->t('The JIRA Endpoint %name has been updated.', $t_args));
    }
    elseif ($status == SAVED_NEW) {
      $this->messenger()->addStatus($this->t('The JIRA Endpoint %name has been added.', $t_args));
    }
    $form_state->setRedirectUrl($this->entity->toUrl('collection'));

    return $status;
  }

  /**
   * Ajax callback for the dependent configuration options.
   *
   * @return array
   *   The form element containing the configuration options.
   */
  public static function ajaxCallback($form, FormStateInterface $form_state) {
    return $form['credentials'];
  }
}
